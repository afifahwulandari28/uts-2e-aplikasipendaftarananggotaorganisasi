package wulandari.fifah.utsfix

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_mhs.*
import kotlinx.android.synthetic.main.frag_data_mhs.view.*


class fragMHs : Fragment(),View.OnClickListener,AdapterView.OnItemSelectedListener {

    lateinit var  v : View
    lateinit var adapSpines : SimpleCursorAdapter
    lateinit var db : SQLiteDatabase
    lateinit var listadap :SimpleCursorAdapter
    lateinit var thisparent : MainActivity
    lateinit var builder :AlertDialog.Builder
    var namajur : String= ""
    var nim_mhs : String=""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.frag_data_mhs,container,false)
        thisparent = activity as MainActivity
        db = thisparent.getDB()
        builder =  AlertDialog.Builder(thisparent)
        v.btnInsert.setOnClickListener(this)
        v.btnUpdate.setOnClickListener(this)
        v.btnDelete.setOnClickListener(this)
        v.lsMhs.setOnItemClickListener(clik)
        v.spinner.onItemSelectedListener = this
        return v

    }

    override fun onStart() {
        super.onStart()
        showData()
        showDatajurusan()
    }
    fun showData(){
        var  sql = "select m.nim as _id , " +
                "m.nama_mhs ,m.alamat,m.nohp ," +
                "p.nama_jurusan from mhs m , " +
                "jurusan p where m.id_jurusan=p.id_jurusan "
        val c : Cursor = db.rawQuery(sql,null)
        listadap = SimpleCursorAdapter(thisparent,R.layout.item_data_mhs,
            c, arrayOf("_id","nama_mhs","alamat","nohp","nama_jurusan"),
            intArrayOf(R.id.nim,R.id.nama,R.id.alamat,R.id.nohp,R.id.jur),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsMhs.adapter = listadap
    }

    fun showDatajurusan() {
        val c: Cursor =
            db.rawQuery("select nama_jurusan as _id from jurusan order by nama_jurusan asc", null)
        adapSpines = SimpleCursorAdapter(
            thisparent,
            android.R.layout.simple_spinner_item,
            c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        adapSpines.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = adapSpines
        v.spinner.setSelection(0)
    }
    fun insertData(nim : String, namaMhs: String, id_prodi: Int, alamat: String, nohp: String) {
        var sql = "insert into mhs(nim, nama_mhs, alamat, nohp, id_jurusan) values (?,?,?,?,?)"
        db.execSQL(sql, arrayOf(nim, namaMhs, alamat, nohp, id_prodi))
        showData()
    }
    fun update(nama_jurusan: String,alamat: String,nohp: String,id_prodi: Int){
        val cv : ContentValues = ContentValues()
        cv.put("nama_mhs",nama_jurusan)
        cv.put("alamat",alamat)
        cv.put("nohp",nohp)
        cv.put("id_jurusan",id_prodi)
        db.update("mhs",cv,"nim = '$nim_mhs'",null)
        showData()

    }

    fun  delete(nim : String){
        db.delete("mhs","nim = '$nim_mhs'",null)
        showData()
    }

    val InsertMHs = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_jurusan from jurusan where nama_jurusan ='$namajur'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            insertData(
                v.edNimMhs.text.toString(), v.edNamaMhs.text.toString(),
                c.getInt(c.getColumnIndex("id_jurusan")),edAlamat.text.toString(),edNo.text.toString()
            )
           v.edNimMhs.setText("")
            v.edNamaMhs.setText("")
            v.edAlamat.setText("")
            v.edNo.setText("")

        }
    }
    val updademhs = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_jurusan from jurusan where nama_jurusan ='$namajur'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            update(
                v.edNamaMhs.text.toString(),
               edAlamat.text.toString(),edNo.text.toString(), c.getInt(c.getColumnIndex("id_jurusan"))
            )
            v.edNimMhs.setText("")
            v.edNamaMhs.setText("")
            v.edAlamat.setText("")
            v.edNo.setText("")

        }
    }
    val btdelet =DialogInterface.OnClickListener { dialog, which ->
        delete(v.edNimMhs.text.toString())
        v.edNimMhs.setText("")
        v.edNamaMhs.setText("")
        v.edAlamat.setText("")
        v.edNo.setText("")

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert->{
                builder.setTitle("Validasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("apa data yang di masukan sudah benar ?")
                    .setPositiveButton("YA",InsertMHs)
                    .setNegativeButton("Tidal",null)
                builder.show()
            }
            R.id.btnUpdate->{
                builder.setTitle("Validasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("apa data yang di masukan sudah benar ?")
                    .setPositiveButton("YA",updademhs)
                    .setNegativeButton("Tidal",null)
                builder.show()
            }
            R.id.btnDelete->{
                builder.setTitle("Validasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("apa data yang di masukan sudah benar ?")
                    .setPositiveButton("YA",btdelet)
                    .setNegativeButton("Tidal",null)
                builder.show()
            }

        }
    }
    val clik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        nim_mhs = c.getString(c.getColumnIndex("_id"))
       // v.edNamaProdi.setText(c.getString(c.getColumnIndex("nama_jurusan")))
        v.edNimMhs.setText(c.getString(c.getColumnIndex("_id")))
        v.edNamaMhs.setText(c.getString(c.getColumnIndex("nama_mhs")))
        v.edAlamat.setText(c.getString(c.getColumnIndex("alamat")))
        v.edNo.setText(c.getString(c.getColumnIndex("nohp")))

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = adapSpines.getItem(position) as Cursor
        namajur = c.getString(c.getColumnIndex("_id"))
    }


}