package wulandari.fifah.utsfix

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_prodi.*
import kotlinx.android.synthetic.main.frag_data_prodi.view.*

class fragprod: Fragment(), View.OnClickListener {

    lateinit var db: SQLiteDatabase
    lateinit var v : View
    lateinit var  thisparent : MainActivity
    lateinit var lisadap : SimpleCursorAdapter
    lateinit var  builder : AlertDialog.Builder
    var id_kat : String =""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.frag_data_prodi,container,false)
        thisparent = activity as MainActivity
        db =  thisparent.getDB()
        builder = AlertDialog.Builder(thisparent)

        v.lsProdi.setOnItemClickListener(clik)
        v.btnDelete.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnUpdate.setOnClickListener(this)
       return v
    }


    fun showjurusan(){
        var sqljur = "select id_jurusan as _id , nama_jurusan from jurusan "
        var c : Cursor = db.rawQuery(sqljur,null)
        lisadap =  SimpleCursorAdapter(thisparent,
            R.layout.item_data_prodi,
            c,
            arrayOf("_id","nama_jurusan"),
            intArrayOf(R.id.txIdProdi,R.id.txNamaProdi),
            SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsProdi.adapter = lisadap
    }

    override fun onStart() {
        super.onStart()
        showjurusan()
    }

    val clik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        id_kat = c.getString(c.getColumnIndex("_id"))
        v.edNamaProdi.setText(c.getString(c.getColumnIndex("nama_jurusan")))

    }

    fun insert(nama_jurusan : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_jurusan",nama_jurusan)
        db.insert("jurusan",null,cv)
        showjurusan()
    }

    fun update(nama_jurusan: String){
        val cv : ContentValues = ContentValues()
        cv.put("nama_jurusan",nama_jurusan)
        db.update("jurusan",cv,"id_jurusan = '$id_kat'",null)
        showjurusan()

    }

    fun  delete(id_jurusan : Int){
        db.delete("jurusan","id_jurusan = '$id_jurusan'",null)
        showjurusan()
    }

    val btinsert = DialogInterface.OnClickListener { dialog, which ->
        insert(edNamaProdi.text.toString())
        v.edNamaProdi.setText("")
    }

    val btupdate = DialogInterface.OnClickListener { dialog, which ->
        update(edNamaProdi.text.toString())

        v.edNamaProdi.setText("")
    }

    val btdelet =DialogInterface.OnClickListener { dialog, which ->
        delete(id_kat.toInt())
        v.edNamaProdi.setText("")
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                builder.setTitle("Validasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("apa data yang di masukan sudah benar ?")
                    .setPositiveButton("YA",btinsert)
                    .setNegativeButton("Tidal",null)
                builder.show()
            }
            R.id.btnUpdate->{
                builder.setTitle("Validasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("apa data yang di masukan sudah benar ?")
                    .setPositiveButton("YA",btupdate)
                    .setNegativeButton("Tidal",null)
                builder.show()
            }
            R.id.btnDelete->{
                builder.setTitle("Validasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("apa data yang di hapus sudah benar ?")
                    .setPositiveButton("YA",btdelet)
                    .setNegativeButton("Tidal",null)
                builder.show()
            }
        }
    }


}